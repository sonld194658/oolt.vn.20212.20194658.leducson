import javax.swing.JOptionPane;

public class FirstDegree {

    public static void main(String[] args) {
        String num1, num2;
        Double n1, n2;
        num1 = JOptionPane.showInputDialog("Please enter a value in (ax+b=0): ");
        if (Double.parseDouble(num1) == 0) {
            JOptionPane.showMessageDialog(null, "a = 0!!!");
            FirstDegree.main(args);
        }
        num2 = JOptionPane.showInputDialog("Please enter b value in (ax+b=0): ");
        n1 = Double.parseDouble(num1);
        n2 = Double.parseDouble(num2);
        JOptionPane.showMessageDialog(null,
                "The equation has a unique solution: " + +(-n2 / n1));
        System.exit(0);
    }
}