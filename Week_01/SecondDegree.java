import javax.swing.JOptionPane;

public class SecondDegree {
    public static void main(String[] args) {
        String aStr, bStr, cStr;
        Double a, b, c, Delta;
        aStr = JOptionPane.showInputDialog(null, "Please enter a value in (ax^2+bx+c=0)");
        if (Double.parseDouble(aStr) == 0) {
            JOptionPane.showMessageDialog(null, "a = 0!!!");
            SecondDegree.main(args);
        }
        bStr = JOptionPane.showInputDialog(null, "Please enter b value in (ax^2+bx+c=0)");
        cStr = JOptionPane.showInputDialog(null, "Please enter c value in (ax^2+bx+c=0)");
        a = Double.parseDouble(aStr);
        b = Double.parseDouble(bStr);
        c = Double.parseDouble(cStr);
        Delta = b * b - 4 * a * c;
        if (Delta < 0) {
            JOptionPane.showMessageDialog(null, "Delta < 0!!!\nVO NGHIEM");
        } else if (Delta == 0) {
            JOptionPane.showMessageDialog(null, "The equation has double root: " + +(-b / (2 * a)));
        } else {
            JOptionPane.showMessageDialog(null, "The equation has two distinct roots: "
                    + +((-b + Math.sqrt(Delta)) / (2 * a)) + " and " + +((-b - Math.sqrt(Delta)) / (2 * a)));
        }
        System.exit(0);
    }
}