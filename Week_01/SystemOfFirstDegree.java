import javax.swing.JOptionPane;

public class SystemOfFirstDegree {
    public static void main(String[] args) {
        String a11Str, a12Str, b1Str, a21Str, a22Str, b2Str;
        Double a11, a12, b1, a21, a22, b2, d1, d2, d;
        a11Str = JOptionPane.showInputDialog(null, "Enter a11 value in \n     (a11)x+a12y=b1\n     a21x+a22y=b2");
        a12Str = JOptionPane.showInputDialog(null, "Enter a12 value in \n     a11x+(a12)y=b1\n     a21x+a22y=b2");
        b1Str = JOptionPane.showInputDialog(null, "Enter b1 value in \n     a11x+a12y=(b1)\n     a21x+a22y=b2");
        a21Str = JOptionPane.showInputDialog(null, "Enter a21 value in \n     a11x+a12y=b1\n     (a21)x+a22y=b2");
        a22Str = JOptionPane.showInputDialog(null, "Enter a22 value in \n     a11x+a12y=b1\n     a21x+(a22y)=b2");
        b2Str = JOptionPane.showInputDialog(null, "Enter b2 value in \n     a11x+a12y=b1\n     a21x+a22y=(b2)");
        a11 = Double.parseDouble(a11Str);
        a12 = Double.parseDouble(a12Str);
        b1 = Double.parseDouble(b1Str);
        a21 = Double.parseDouble(a21Str);
        a22 = Double.parseDouble(a22Str);
        b2 = Double.parseDouble(b2Str);
        d = a11 * a22 - a21 * a12;
        d1 = b1 * a22 - b2 * a12;
        d2 = b2 * a11 - b1 * a21;
        if (d == 0) {
            if (d1 != 0 || d2 != 0)
                JOptionPane.showMessageDialog(null, "The system has no solution");
            else
                JOptionPane.showMessageDialog(null, "The systemhas infinitely many solutions");
        } else
            JOptionPane.showMessageDialog(null, "The system has a unique solution: (x,y)=(" + (d1 / d) + ";" + (d2 / d)+")");
        System.exit(0);
    }
}
