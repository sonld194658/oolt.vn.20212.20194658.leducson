public class AddTwoMatrices {
    public static void main(String[] args) {
        float matrix1[][] = { { 1.1f, 1.2f, 1.3f, 1.4f }, { 2.3f, 2.4f, 2.5f, 2.6f }, { 3.5f, 3.6f, 3.7f, 3.8f } },
                matrix2[][] = { { 5.2f, 5.3f, 5.4f, 5.5f }, { 6.3f, 6.4f, 6.5f, 6.6f }, { 7.4f, 7.5f, 7.5f, 7.6f } },
                matrix3[][] = new float[3][4];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                matrix3[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        System.out.println("Matrix 1:");
        for (int i = 0; i < 3; i++) {
            for (float j : matrix1[i]) {
                System.out.printf("%.1f\t", j);
            }
            System.out.println();
        }
        System.out.println("Matrix 2:");
        for (int i = 0; i < 3; i++) {
            for (float j : matrix2[i]) {
                System.out.printf("%.1f\t", j);
            }
            System.out.println();
        }
        System.out.println("Matrix 3 = Matrix1 + Matrix2:");
        for (int i = 0; i < 3; i++) {
            for (float j : matrix3[i]) {
                System.out.printf("%.1f\t", j);
            }
            System.out.println();
        }
    }
}