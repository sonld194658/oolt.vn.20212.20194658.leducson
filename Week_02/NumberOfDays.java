import java.util.Scanner;

public class NumberOfDays {
    public static void main(String[] args) {
        final String month[][] = {
                { "January", "Jan.", "Jan", "1" }, // Tháng 1
                { "February", "Feb.", "Feb", "2" }, // Tháng 2
                { "March", "Mar.", "Mar", "3" }, // Tháng 3
                { "April", "Apr.", "Apr", "4" }, // Tháng 4
                { "May", "5", "5", "5" }, // Tháng 5
                { "June", "Jun", "6", "6" }, // Tháng 6
                { "July", "Jul", "7", "7" }, // Tháng 7
                { "August", "Aug.", "Aug", "8" }, // Tháng 8
                { "September", "Sept.", "Sep", "9" }, // Tháng 9
                { "October", "Oct.", "Oct", "10" }, // Tháng 10
                { "November", "Nov.", "Nov", "11" }, // Tháng 11
                { "December", "Dec.", "Dec", "12" }// Tháng 12
        };
        final int numberofdays[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// Các ngày trong tháng của năm
                                                                                      // không nhuận
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a year: ");
        String syear;
        // Kiem tra xem nam co dung dinh dang hay khong *1
        int dem = 0, year;
        do {// Kiểm tra xem string có phải dạng số không
            syear = scanner.nextLine();
            for (dem = 0; dem < syear.length(); dem++) {
                if (syear.charAt(dem) < '0' || syear.charAt(dem) > '9')// Kiểm tra xem từng ký tự trong string có phải
                                                                       // chữ số không
                    break;
            }
            if (dem == syear.length() && dem != 0) {// Nếu tất cả các ký tự trong string đều là số
                year = Integer.parseInt(syear);
                if (year > 0)// Năm phải là số dương
                    break;
            }
            System.out.print("Please try again enter a year: ");
        } while (true);
        // end *1

        // Kiem tra co phai la nam nhuan hay khong *2
        if (year % 4 == 0) { // Là năm nhuận nếu chia hết cho 4
            if (year % 100 == 0) { // ngoại trừ nếu chia hết cho 100
                if (year % 400 != 0) { // và không chia hết cho 400
                    year = 1;// biến year không cần dùng đến nữa nên tận dụng làm giá trị nhận biết năm nhuận
                             // 1: nhuận, 0: không nhuận
                } else
                    year = 0;
            } else
                year = 1;
        } else
            year = 0;
        // end *2
        System.out.print("Please enter a month: ");
        String amonth;
        int i;// Giá trị cuối cùng của biến i là giá trị trả về của tháng tương ứng trừ 1 nếu
              // thõa mãn định dạng
        loop1: do {
            amonth = scanner.nextLine();
            for (i = 0; i < 12; i++) {
                for (int j = 0; j < 4; j++) {
                    if (amonth.equals(month[i][j]))// So sánh định dạng có thỏa mãn với các định dạng cho trước hay
                                                   // không
                        break loop1;
                }
            }
            System.out.print("Please try again enter a month: ");
        } while (true);
        System.out.println("The number of days of month: "
                + (year == 0 ? numberofdays[i] : (i == 1 ? numberofdays[i] + 1 : numberofdays[i])));// Nếu không phải
                                                                                                    // năm nhuận, trả về
                                                                                                    // số ngày như bình
                                                                                                    // thường. Nếu là
                                                                                                    // năm nhuận, kiểm
                                                                                                    // tra xem nó có
                                                                                                    // phải tháng 2 hay
                                                                                                    // không, nếu là
                                                                                                    // tháng 2 thì trả
                                                                                                    // về số ngày bình
                                                                                                    // thường công thêm
                                                                                                    // 1, ngược lại như
                                                                                                    // trả về số ngày
                                                                                                    // bình thường
        scanner.close();
    }
}