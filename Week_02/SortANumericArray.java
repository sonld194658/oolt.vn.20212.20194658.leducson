import java.util.Arrays;

public class SortANumericArray {
    public static void main(String[] args) {
        int array[] = { 4901, 1101, 1591, 30121, 2181 };
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }
}