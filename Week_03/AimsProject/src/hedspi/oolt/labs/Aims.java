package hedspi.oolt.labs;

public class Aims {

  public static void main(String[] args) {
    DigitalVideoDisc dvd1 = new DigitalVideoDisc();
    DigitalVideoDisc dvd2 = new DigitalVideoDisc("The lion king");
    dvd1.setCategory("Animation");
    dvd1.setDirector("Forger Alerts");
    dvd1.setDirector("George Lucas");
    dvd1.setTitle("Big hand");
    dvd1.setLength(124);
    dvd1.setCost(50f);
    DigitalVideoDisc dvd3 = new DigitalVideoDisc(
        "Star wars", "Science Fiction", "George Lucas",
        124, 24.95f);
    //Tao mot don hang moi
    Order anOrder = new Order();
    //Them cac DVD vao trong don hang
    anOrder.addDigitalVideoDisc(dvd1);
    anOrder.addDigitalVideoDisc(dvd2);
    anOrder.addDigitalVideoDisc(dvd3);
    anOrder.printOrder();
    //In ra tong gia tri don hang
    System.out.println("Total cost is: " + anOrder.totalCost() + "$");
    anOrder.removeDigitalVideoDisc(dvd1);
    anOrder.removeDigitalVideoDisc(dvd2);
    anOrder.removeDigitalVideoDisc(dvd2);
    anOrder.removeDigitalVideoDisc(dvd2);
    anOrder.printOrder();
    System.out.println("Total cost is: " + anOrder.totalCost() + "$");
  }
}
