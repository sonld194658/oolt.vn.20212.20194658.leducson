package hedspi.oolt.labs;

public class Order {

  //Khai bao hang so: so luong toi da
  public static final int MAX_NUMBERS_ORDERED = 10;
  //Khai bao mang cac doi tuong DVD
  private final DigitalVideoDisc[] itemsOrdered = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
  //Khai bao thuoc tinh chua so luong so phan tu hien co trong don hang
  private int qtyOrdered = 0;

  //Xay dung cac phuong thuc get/set
  public int getQtyOrdered() {
    return qtyOrdered;
  }

  public void setQtyOrdered(int qtyOrdered) {
    this.qtyOrdered = Math.max(qtyOrdered, 0);
  }

  //Xay dung phuong thuc them mot doi tuong DVD vao don hang
  //Noi dung: them mot doi tuong vao mang itemOrdered
  //--> phai kiem tra xem mang da day chua?
  public void addDigitalVideoDisc(DigitalVideoDisc disc) {
    if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
      System.out.println("The order is almost full");
    } else {
      this.itemsOrdered[qtyOrdered++] = disc;
      System.out.println("The disc has been added");
      System.out.println("Total disc: " + this.qtyOrdered);
    }
  }

  //Phuong thuc xoa DVD khoi Order
  public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
    //Viet lenh loai bo doi tuong DVD khoi mang
    //Luu y kiem tra tinh huong mang rong (khong co phan tu)
    if (qtyOrdered == 0) {
      System.out.println("The order is empty");
      return;
    }
    int position = qtyOrdered;
    for (int i = 0; i < qtyOrdered; i++) {
      if (this.itemsOrdered[i].equals(disc)) {
        position = i;
        System.out.println("Remove successful. Current length: " + (qtyOrdered - 1));
        break;
      }
    }
    if (position == qtyOrdered) {
      System.out.println("Can't find DVD in order");
      return;
    }
    if (qtyOrdered - 1 - position >= 0) {
      System.arraycopy(this.itemsOrdered, position + 1, this.itemsOrdered, position,
          qtyOrdered - 1 - position);
    }
    qtyOrdered--;
  }

  public void printOrder() {
    if (qtyOrdered == 0) {
      return;
    }
    System.out.println("------------Order------------");
    for (int i = 0; i < qtyOrdered; i++) {
      System.out.println("Item " + (i + 1));
      this.itemsOrdered[i].printInfo();
    }
    System.out.println("------------End Order------------\n");
  }

  //Phuong thuc tinh tong gia tri cua don hang
  public float totalCost() {
    float total = 0.0f;
    for (int i = 0; i < qtyOrdered; i++) {
      total += itemsOrdered[i].getCost();
    }
    return total;
  }
}
