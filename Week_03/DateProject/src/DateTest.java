import java.util.Scanner;

public class DateTest {
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);
    MyDate date = new MyDate();
    MyDate date1 = new MyDate(2020, "April", "1st");
    MyDate date2 = new MyDate("April \n 2nd    2020");
    date.printDate();
    date1.printDate();
    date2.printDate();
    date.accept(scanner);
    MyDate.print();
    scanner.close();
    date.printDate();
  }
}
