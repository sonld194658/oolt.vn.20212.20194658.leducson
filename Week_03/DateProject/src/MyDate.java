import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class MyDate {

  private static final String[] strDay = new String[]{
      "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th", "10th",
      "11th", "12th", "13th", "14th", "15th", "16th", "17th", "18th", "19th", "20th",
      "21th", "22th", "23th", "24th", "25th", "26th", "27th", "28th", "29th", "30th", "31th"};
  private static final String[] strMonth = new String[]{"January", "February", "March", "April",
      "May", "June", "July", "August", "September", "October", "November", "December"};
  private int year;
  private String month;
  private String day;

  public String getMonth() {
    return month;
  }

  public void setMonth(String month) {
    for (int i = 0; i < 12; i++) {
      if (month.equals(strMonth[i])) {
        this.month = month;
        break;
      }
    }
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = Math.max(year, 1900);
  }

  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    for (int i = 0; i < 31; i++) {
      if (day.equals(strDay[i])) {
        this.day = day;
        break;
      }
    }
  }

  public MyDate() {
    super();
    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
    setYear(Integer.parseInt(formatter.format(date)));
    formatter.applyPattern("MMMM");
    setMonth(formatter.format(date));
    formatter.applyPattern("dd");
    setDay(strDay[Integer.parseInt(formatter.format(date)) - 1]);
  }

  public MyDate(int year, String month, String day) {
    super();
    setYear(year);
    setMonth(month);
    setDay(day);
  }

  public MyDate(String format) {
    super();
    try {
      String[] cut = format.replaceAll("\\s+", " ").split("\\s");
      setYear(Integer.parseInt(cut[2]));
      setDay(cut[1]);
      setMonth(cut[0]);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public void accept(Scanner scanner) {
    System.out.println("Please enter a date (MMMM DDDD YYYY):");
    String aDate = scanner.nextLine();
    try {
      String[] cut = aDate.replaceAll("\\s+", " ").split("\\s");
      setYear(Integer.parseInt(cut[2]));
      setDay(cut[1]);
      setMonth(cut[0]);
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public static void print() {
    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("MMMM");
    System.out.print("Current date: " + formatter.format(date) + " ");
    formatter.applyPattern("dd");
    System.out.print(strDay[Integer.parseInt(formatter.format(date)) - 1] + " ");
    formatter.applyPattern("yyyy");
    System.out.println(formatter.format(date));
  }

  public void printDate() {
    System.out.println("Print Date: "+this.month + " " + this.day + " " + this.year);
  }
}
