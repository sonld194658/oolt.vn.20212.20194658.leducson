package hedspi.oolt.labs;

public class Aims {

  public static void main(String[] args) {
    DigitalVideoDisc dvd1 = new DigitalVideoDisc(
        "Big hand", "Animation", "Forger Alerts", 124, 30.4f);
    DigitalVideoDisc dvd2 = new DigitalVideoDisc(
        "The lion king", "Animation", "Le Duc Son", 100, 25f);
    DigitalVideoDisc dvd3 = new DigitalVideoDisc(
        "Star wars", "Science Fiction", "George Lucas", 124, 24.95f);
    DigitalVideoDisc dvd4 = new DigitalVideoDisc(
      "Title_4", "Category_4", "Director_4", 125, 20.9f
    );
    DigitalVideoDisc dvd5 = new DigitalVideoDisc(
        "Title_5", "Category_5", "Director_5", 125, 20.9f
    );
    DigitalVideoDisc dvd6 = new DigitalVideoDisc(
        "Title_6", "Category_6", "Director_6", 125, 20.9f
    );
    DigitalVideoDisc dvd7 = new DigitalVideoDisc(
        "Title_7", "Category_7", "Director_7", 125, 20.9f
    );
    DigitalVideoDisc dvd8 = new DigitalVideoDisc(
        "Title_8", "Category_8", "Director_8", 125, 20.9f
    );
    DigitalVideoDisc dvd9 = new DigitalVideoDisc(
        "Title_9", "Category_9", "Director_9", 125, 20.9f
    );
    DigitalVideoDisc dvd10 = new DigitalVideoDisc(
        "Title_10", "Category_10", "Director_10", 125, 20.9f
    );
    DigitalVideoDisc[] listDVD1 = new DigitalVideoDisc[]{dvd1, dvd2, dvd3};
    DigitalVideoDisc[] listDVD2 = new DigitalVideoDisc[]{dvd4, dvd5, dvd6};
    DigitalVideoDisc[] listDVD3 = new DigitalVideoDisc[]{dvd7, dvd8, dvd9};
    DigitalVideoDisc[] listDVD4 = new DigitalVideoDisc[]{dvd10, dvd1, dvd2};
    DigitalVideoDisc[] listDVD5 = new DigitalVideoDisc[]{dvd3, dvd4, dvd5};
    //Tao mot don hang moi
    Order anOrder = new Order();
    Order anOrder1 = new Order();
    Order anOrder2 = new Order();
    Order anOrder3 = new Order();
    Order anOrder4 = new Order();
    Order anOrder5 = new Order();

//    Them cac DVD vao trong don hang
    anOrder.addDigitalVideoDisc(listDVD1);
    anOrder.printOrder();
    anOrder1.addDigitalVideoDisc(listDVD2);
    anOrder1.printOrder();
    anOrder2.addDigitalVideoDisc(listDVD3);
    anOrder2.printOrder();
    anOrder3.addDigitalVideoDisc(listDVD4);
    anOrder3.printOrder();
    anOrder4.addDigitalVideoDisc(listDVD5);
    anOrder4.printOrder();
    //In ra tong gia tri don hang
//    anOrder.removeDigitalVideoDisc(dvd1);
//    anOrder.removeDigitalVideoDisc(dvd2);
//    anOrder.removeDigitalVideoDisc(dvd2);
//    anOrder.removeDigitalVideoDisc(dvd2);
//    anOrder.printOrder();
  }
}
