package hedspi.oolt.labs;

import java.util.Objects;

public class DigitalVideoDisc {

  private String title;
  private String category;
  private String director;
  private int length;
  private float cost;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getDirector() {
    return director;
  }

  public void setDirector(String director) {
    this.director = director;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = Math.max(length, 0);
  }

  public float getCost() {
    return cost;
  }

  public void setCost(float cost) {
    this.cost = Math.max(cost, 0);
  }

  public DigitalVideoDisc() {
    super();
  }

  public DigitalVideoDisc(String title) {
    setTitle(title);
  }

  public DigitalVideoDisc(String title, String category) {
    this(title);
    setCategory(category);
  }

  public DigitalVideoDisc(String title, String category, String director) {
    this(title, category);
    setDirector(director);
  }

  public DigitalVideoDisc(String title, String category, String director, int length) {
    this(title, category, director);
    setLength(length);
  }

  public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
    this(title, category, director, length);
    setCost(cost);
  }

  //3 Cac phuong thuc khac
  //Phuong thuc in thong tin cua doi tuong dvd
  public void printInfo() {
    System.out.println("------DVD Info-------");
    System.out.println("Title: " + this.title);
    System.out.println("Category: " + this.category);
    System.out.println("Director: " + this.director);
    System.out.println("Length: " + this.length);
    System.out.println("Cost: " + this.cost);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalVideoDisc that = (DigitalVideoDisc) o;
    return length == that.length && Float.compare(that.cost, cost) == 0 && Objects.equals(title,
        that.title) && Objects.equals(category, that.category) && Objects.equals(director,
        that.director);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, category, director, length, cost);
  }
}
