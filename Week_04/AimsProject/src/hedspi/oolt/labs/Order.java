package hedspi.oolt.labs;


public class Order {

  //Khai bao hang so: so luong toi da
  public static final int MAX_NUMBERS_ORDERED = 10;
  public static final int MAX_LIMITTED_ORDERS = 5;
  private static int nbOrders = 0;
  //Khai bao mang cac doi tuong DVD
  private DigitalVideoDisc[] itemsOrdered;
  //Khai bao thuoc tinh chua so luong so phan tu hien co trong don hang
  private int qtyOrdered = 0;
  private MyDate dateOrdered;

  public Order() {
    if (nbOrders < MAX_LIMITTED_ORDERS) {
      this.dateOrdered = new MyDate();
      this.itemsOrdered = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
      nbOrders++;
    } else {
      System.out.println("The numbers order is almost full");
    }
  }


  public MyDate getDateOrdered() {
    return dateOrdered;
  }

  public void setDateOrdered(MyDate dateOrdered) {
    this.dateOrdered = dateOrdered;
  }

  //Xay dung cac phuong thuc get/set
  public int getQtyOrdered() {
    return qtyOrdered;
  }

  public void setQtyOrdered(int qtyOrdered) {
    this.qtyOrdered = Math.max(qtyOrdered, 0);
  }

  //Xay dung phuong thuc them mot doi tuong DVD vao don hang
  //Noi dung: them mot doi tuong vao mang itemOrdered
  //--> phai kiem tra xem mang da day chua?
  public void addDigitalVideoDisc(DigitalVideoDisc disc) {
    if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
      System.out.println("The order is almost full");
    } else {
      this.itemsOrdered[qtyOrdered++] = disc;
      System.out.println("The disc has been added");
      System.out.println("Total disc: " + this.qtyOrdered);
    }
  }

  public void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
    if (this.qtyOrdered + 2 > MAX_NUMBERS_ORDERED) {
      System.out.println("The order is almost full");
    } else {
      this.addDigitalVideoDisc(disc1);
      this.addDigitalVideoDisc(disc2);
    }
  }

  public void addDigitalVideoDisc(DigitalVideoDisc[] listDisc) {
    if (this.qtyOrdered == MAX_NUMBERS_ORDERED
        || this.qtyOrdered + listDisc.length > MAX_NUMBERS_ORDERED) {
      System.out.println("The order is almost full");
    } else {
      for (DigitalVideoDisc disc : listDisc) {
        this.addDigitalVideoDisc(disc);
      }
    }
  }

  //Phuong thuc xoa DVD khoi Order
  public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
    //Viet lenh loai bo doi tuong DVD khoi mang
    //Luu y kiem tra tinh huong mang rong (khong co phan tu)
    if (qtyOrdered == 0) {
      System.out.println("The order is empty");
      return;
    }
    int position = qtyOrdered;
    for (int i = 0; i < qtyOrdered; i++) {
      if (this.itemsOrdered[i].equals(disc)) {
        position = i;
        System.out.println("Remove successful. Current length: " + (qtyOrdered - 1));
        break;
      }
    }
    if (position == qtyOrdered) {
      System.out.println("Can't find DVD in order");
      return;
    }
    if (qtyOrdered - 1 - position >= 0) {
      System.arraycopy(this.itemsOrdered, position + 1, this.itemsOrdered, position,
          qtyOrdered - 1 - position);
    }
    qtyOrdered--;
  }

  public void printOrder() {
    if (qtyOrdered == 0) {
      return;
    }
    DigitalVideoDisc dvd;
    System.out.println("**************************ORDER**************************");
    System.out.print("Date: ");//MMMM dd yyyy
    this.dateOrdered.printDate("MMMM-dd-yyyy");
    System.out.println("Ordered Items:");
    for (int i = 0; i < qtyOrdered; i++) {
      dvd = this.itemsOrdered[i];
      System.out.println((i + 1) + ". " +
          "DVD - " + dvd.getTitle() + " - " + dvd.getCategory() + " - " + dvd.getDirector() + " - "
          + dvd.getLength() + ": " + dvd.getCost() + "$");
    }
    System.out.println("Total cost: " + this.totalCost() + "$");
    System.out.println("*********************************************************\n");
  }

  //Phuong thuc tinh tong gia tri cua don hang
  public float totalCost() {
    float total = 0.0f;
    for (int i = 0; i < qtyOrdered; i++) {
      total += itemsOrdered[i].getCost();
    }
    return total;
  }
}
