package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {

  private List<String> authors = new ArrayList<>();

  public Book(String title) {
    super(title);
  }

  public Book(String title, String category) {
    super(title, category);
  }

  public Book(String title, String category, List<String> authors) {
    super(title, category);
    this.authors = authors;
  }

  public Book(String title, String category, float cost, List<String> authors) {

    super(title, category, cost);
    this.authors = authors;

  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }


  public void addAuthor(String authorName) {
    if (!authors.contains(authorName)) {
      authors.add(authorName);
    }
  }

  public void removeAuthor(String authorName) {
//    if (authors.contains(authorName)){
//      authors.remove(authorName);
//    }
    authors.remove(authorName);
  }

  @Override
  public void print() {
    System.out.println("------Book Info-------");
    System.out.println("Title: " + this.title);
    System.out.println("Category: " + this.category);
    System.out.print("Authors: ");
    this.authors.forEach(e -> {
      System.out.print(e + "-");
    });
    System.out.println("\nCost: " + this.cost);
    System.out.println("----------------------");
  }
}