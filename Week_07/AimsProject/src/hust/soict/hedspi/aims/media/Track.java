package hust.soict.hedspi.aims.media;

public class Track implements PlayAble {

  private String title;
  private int length;

  public String getTitle() {
    return title;
  }

  public Track(String title, int length) {
    this.title = title;
    this.length = length;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public Track() {
    this.title = "";
    this.length = 0;
  }

  @Override
  public void play() {
    System.out.println("Play Track: " + this.getTitle());
    System.out.println("Track length: " + this.getLength());
  }
}
